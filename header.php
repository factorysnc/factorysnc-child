<!DOCTYPE html>
<!--[if IE 9]> <html class="no-js ie9 fixed-layout" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js " lang="en"> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<title><?php bloginfo( 'name' ); ?></title>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div id="wrapper" class="animsition">
		<?php $image = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ), 'full' );?>
		<nav class="navbar navbar-light bg-faded  navbar-toggleable-md sticky-top">
			<?php if( function_exists( 'the_custom_logo' ) && has_custom_logo() ){ the_custom_logo(); }?>
			<!-- <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation"> -->
				<!-- <span class="navbar-toggler-icon"></span> -->
				<!-- <i class="fa fa-bars" aria-hidden="true"></i> -->
				<!-- </button> -->

				<button id="menu-toggle" class="navbar-toggler navbar-toggler-right hamburger hamburger--collapse" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>

				<div id="menu-overlay" class="overlay">
					<h2 class="nav-title">Navigation</h2>
					<nav class="overlay-menu">
						<?php
						wp_nav_menu( array(
							'theme_location' => 'primary',
							// 'menu_class'     => 'nav navbar-nav navbar-right',
							// 'container_id'   => 'navbar',
							// 'container_class' => 'navbar-collapse collapse',
						));
						?> 
					</nav>
				</div>
			</nav>
