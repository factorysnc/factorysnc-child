	<div id="footer-wrapper">
		<footer class="py-5">
			<div class="container">
				<div class="row">
					<div class="col mx-auto text-center">
						<!-- <p><a href="mailto:info@pagnonigroup.com"><span>info@pagnonigroup.com</span></a></p> -->
						<p class="mb-4">
							<a href="#"><i class="fa fa-facebook circle mx-1" aria-hidden="true"></i></a>
							<a href="#"><i class="fa fa-vimeo circle mx-1" aria-hidden="true"></i></a>
							<a href="#"><i class="fa fa-instagram circle mx-1" aria-hidden="true"></i></a>
						</p>
						<!-- <p><a href="mailto:info@pagnonigroup.com"><i class="fa fa-envelope-o mr-2" aria-hidden="true"></i><span>info@pagnonigroup.com</span></a></p> -->
						<!-- <p><a href="tel:0000000000"><i class="fa fa-phone mr-2" aria-hidden="true"></i><span>0000 000000</span></a></p> -->
						<!-- <p><span><small>Design by</span><a href="https://www.factorysnc.com" target="_blank"> factory snc</a></small></p> -->
						<p><span><small>Pagnoni Group Srl. © <?php echo date('Y');?></small></span><span><small> - Design by</span><a href="https://www.factorysnc.com" target="_blank"> factorysnc</a></small></p>
						
					</div>
				</div>
			</div>
		</footer>
	</div>
	<?php
	if(wp_script_is( "factorysnc-photoswipe-js", 'enqueued' )){
		include get_template_directory() . '/assets/php/factorysnc-photoswipe.php';
	}
	?>
</div><!-- end wrapper -->
<?php wp_footer(); ?>
</body>
</html>