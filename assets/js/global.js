jQuery(document).ready(function($){
	var $margin = $( '#footer-wrapper' ).innerHeight();
	var $article = $( '#wrapper article' );
	var $menuToggle = $('#menu-toggle');
	var $menuOverlay = $('#menu-overlay');
	var $body = $('html, body');

	$article.css( 'margin-bottom' , $margin );
	$menuToggle.click(function(){
		$(this).toggleClass( 'is-active' );
		// $body.toggleClass( 'noscroll' );
		$menuOverlay.toggleClass( 'open' );
	});
});