<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-xl-8 col-lg-9 col-md-10 col-sm-11 mx-auto my-5">
					<h1 class="display-2 mb-5"><?php the_title(); ?></h1>
					<?php the_content(); ?>
					<?php
					$content_images = array();
					$content_images = factorysnc_get_content_images();
					
					$photoswipe_data = array(
						'photoswipe_elements' => $content_images,
						'photoswipe_elements_selector' => '.'.PHOTOSWIPE_ELEMENTS_CLASS_NAME,
					);
					wp_localize_script( 'factorysnc-photoswipe-js', 'photoswipe_data', $photoswipe_data );
					?>
				</div>
			</div>
		</div>
	</article>
<?php endwhile;?>
<?php get_sidebar();?>
<?php get_footer();?>