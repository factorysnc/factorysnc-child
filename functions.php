<?php
add_action( 'wp_enqueue_scripts', 'factorysnc_child_enqueue_styles' );
function factorysnc_child_enqueue_styles() {

	wp_register_script( 'factorysnc-jarallax-js', get_stylesheet_directory_uri() . '/assets/js/factorysnc-jarallax.js' , array( 'jarallax-js' ), false, false );

	wp_register_script( 'factorysnc-child-main-js' , get_stylesheet_directory_uri() . '/assets/js/global.js' , array( 'jquery' ) , false, false );

	wp_enqueue_style( 'factorysnc-child-css', get_stylesheet_uri() );

	wp_enqueue_script( 'factorysnc-child-main-js' );

	if(is_page()){
		wp_enqueue_script( 'factorysnc-jarallax-js' );
	}
}