<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="page-image jarallax animated">
						<?php echo get_the_post_thumbnail( get_the_ID(), 'full', array( 'class' => 'jarallax-img' ) );?>
						<h1 class="display-1"><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-xl-8 col-lg-9 col-md-10 col-sm-11 mx-auto my-5">
					<h2 class="display-4 mb-5"><?php echo factorysnc_get_the_excerpt(get_the_excerpt());?></h2>
					<?php the_content();?>

					<?php
					$content_images = array();
					$gallery_images = array();
					$content_images = factorysnc_get_content_images();
					$gallery_images = factorysnc_get_gallery_images('full');
					$count_content_images = count($content_images);

					$data_index = count($content_images);
					$photoswipe_elements = array_merge($content_images , $gallery_images);
					
					$photoswipe_data = array(
						'photoswipe_elements' => $photoswipe_elements,
						'photoswipe_elements_selector' => '.'.PHOTOSWIPE_ELEMENTS_CLASS_NAME,
					);
					wp_localize_script( 'factorysnc-photoswipe-js', 'photoswipe_data', $photoswipe_data );
					?>
				</div>
			</div>
			<div class="row no-gutters page-gallery">
				<?php foreach ($gallery_images as $item):?>
					<div class="wrapper col-xl-3 col-lg-4 col-sm-6 col-12">
						<img data-index="<?php echo $data_index++ ?>" class="img-fluid photoswipe-img" src="<?php echo $item['src']?>">
						<div class="overlay">
							<div class="text">
								<h6>Lorem</h6>
								<span><small>Ipsum</small></span>
							</div>
						</div>
					</div>
				<?php endforeach;?>
			</div>
		</div>
	</article>
<?php endwhile;?>
<?php get_sidebar();?>
<?php get_footer();?>